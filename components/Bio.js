import React from 'react'
import { config } from 'config'
import { rhythm } from 'utils/typography'
import { prefixLink } from 'gatsby-helpers'

class Bio extends React.Component {
  render () {
    return (
      <div>
        <p
          style={{
            marginBottom: rhythm(2.5),
          }}
        >
          <img
            alt="face"
            src={prefixLink('/aerobatic-airplane.png')}
            style={{
              float: 'left',
              marginRight: rhythm(1/4),
              marginBottom: 0,
              height: rhythm(2),
            }}
          />
          Written by <strong>{config.authorName}</strong> who work in
          Seattle building the best static hosting system EVAR.
        </p>
        <p>
          <a href="https://twitter.com/aerobaticapp">You should follow them on Twitter</a>
        </p>
      </div>
    )
  }
}

export default Bio
